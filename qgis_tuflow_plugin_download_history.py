import requests
import bs4
from dateutil.parser import parse
import plotly.graph_objects as go
from datetime import datetime, timedelta
from pathlib import Path


URL = 'https://plugins.qgis.org/plugins/tuflow/'
SAVE_FIG = True
SHOW_FIG = True


def main():
    r = requests.get(URL)
    if r.status_code != requests.codes.ok:
        r.raise_for_status()

    soup = bs4.BeautifulSoup(r.content, 'html.parser')
    table = soup.find_all('table')[0].find_all('tbody')[0]
    info = []
    for row in table.children:
        if type(row) is not bs4.element.Tag:
            continue
        info.append([x.text for x in row.children if type(x) == bs4.element.Tag])

    # collect download counts from table - (count, date, min qgis version)
    counts = sorted([(int(x[3]), parse(x[-1]), x[2]) for x in info], key=lambda x: x[1])

    # need to do some post processing on this data because we supported 2 versions of the plugin
    # for a while (one for qgis 2, another for qgis 3). And so the dates aren't in order and there
    # are 2 releases on some dates. Just sum the download counts from these instances together.
    xdata = []
    counts_corrected = []
    skip = False
    for i, (count, date, qgis_version) in enumerate(counts[:-1]):
        count_next, date_next, qgis_version_next = counts[i+1]
        if not skip:
            # released on the same day and the minimum qgis version is different
            if abs((date - date_next).total_seconds() / 3600 / 24) < 1 and qgis_version != qgis_version_next:
                xdata.append(date)
                counts_corrected.append(count + count_next)
                skip = True
            else:
                xdata.append(date)
                counts_corrected.append(count)
                skip = False
        else:
            skip = False
    xdata.append(counts[-1][1])
    counts_corrected.append(counts[-1][0])

    # create y-series of cumulative download counts
    cumulative = 0
    ydata = []
    for count in counts_corrected:
        cumulative += count
        ydata.append(cumulative)

    # pull out key dates
    first_qgis3_version_x = datetime(2018, 2, 7, 23, 42)
    first_qgis3_version_y = ydata[xdata.index(first_qgis3_version_x)]
    first_tuflow_viewer_x = datetime(2019, 2, 22, 0, 22)
    first_tuflow_viewer_y = ydata[xdata.index(first_tuflow_viewer_x)]

    # plotly figure stuff
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=xdata, y=ydata,
                             mode='lines+markers',
                             name='TUFLOW Plugin Download Count',
                             marker_size=10,
                             marker_color='#00537F'))
    fig.add_trace(go.Scatter(x=[first_qgis3_version_x, first_tuflow_viewer_x],
                             y=[first_qgis3_version_y, first_tuflow_viewer_y],
                             text=['First QGIS 3 compatible version', 'TUFLOW Viewer released'],
                             textposition="bottom right",
                             mode='markers',
                             marker_size=11,
                             marker_color='#E20478'))
    fig.add_annotation(x=first_qgis3_version_x + timedelta(days=10), y=first_qgis3_version_y - 800,
                       text='First QGIS 3 compatible version',
                       showarrow=False,
                       bordercolor='black',
                       borderwidth=1,
                       bgcolor="white",
                       xanchor='left',
                       yanchor='top',
                       borderpad=4
                       )
    fig.add_annotation(x=first_tuflow_viewer_x + timedelta(days=10), y=first_tuflow_viewer_y - 800,
                       text='TUFLOW Viewer released',
                       showarrow=False,
                       bordercolor='black',
                       borderwidth=1,
                       bgcolor="white",
                       xanchor='left',
                       yanchor='top',
                       borderpad=4
                       )
    fig.update_traces(showlegend=False)
    fig.update_layout(
        title={
            'text': f"TUFLOW Plugin Download Count: current count = {cumulative:,}",
            'y': 0.95,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        xaxis_title="Date",
        yaxis_title="Download Count",
        font=dict(
            family="Arial",
            size=18,
            color="Black"
        ),
        autosize=False,
        width=1080,
        height=720,
    )

    if SHOW_FIG:
        fig.show()
    if SAVE_FIG:
        if not Path('images').exists():
            Path('images').mkdir()
        fig.write_image("images/TUFLOW_Plugin_Download_Count.png")


if __name__ == '__main__':
    main()
