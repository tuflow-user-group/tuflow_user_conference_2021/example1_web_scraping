# Example 1 - Web Scraping
This project is a copy of the first example presented at the TUFLOW user conference 2021 in the Python workshop. This project scrapes the download counts for the QGIS TUFLOW plugin from the QGIS plugin repository and plots the cumulative total vs time with custom annotations.

After scraping the download count the data is post-processed:

- The download count is cumulated
- Releases that were concurrent in QGIS 2 and QGIS 3 are added together for that particular release

## Installation
1. Clone this repository
```
git clone https://gitlab.com/tuflow-user-group/tuflow_user_conference_2021/example1_web_scraping.git
```
2. Install the dependencies
```
pip install -r requirements.txt
```

## Dependencies
Python 3.9+ should be used. It may run on earlier versions however has not been tested. For a list of dependencies, please see the `requirements.txt`.

## Usage
To run the script:

1. open `qgis_tuflow_plugin_download_history.py`
2. set `SAVE_FIG` and `SHOW_FIG` parameters as desired
   1. if `SAVE_FIG` is set to `True` then the figure will be saved as a png to a folder named 'images' in the project directory
   2. if `SHOW_FIG` is set to `True` then the figure will be shown to the user once the script is complete
3. run the script

The created image should look similar to this:

![TUFLOW_Plugin_Download_Count](/uploads/134538fdccb849b83da79077cf7cd273/TUFLOW_Plugin_Download_Count.png)

## Support
For any issues please get into contact with [support@tuflow.com](mailto:support@tuflow.com).

## License
This project is licensed under GNU GENERAL PUBLIC LICENSE Version 3.
